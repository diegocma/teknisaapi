<?php

namespace App\Http\Controllers;

use App\Validations\Email;
use Illuminate\Http\Request;

class TekController extends Controller
{

    public function __construct()
    {

    }

    public function store(Request $request)
    {
        $recebeEM = $request -> getContent("emails");

        $handle = fopen("temp.txt", "w"); //Gravando arquivo temporário para verificação.
        fwrite($handle, $recebeEM);
        {
            echo "Arquivo Temp Salvo corretamente.\n";
        }
        fclose($handle);

        $mail = new Email(); //Instância da classe e-mail em $mail
        $mail = $mail -> Filter(); //Filtra os e-mails recebidos via post/request.

        $mailSemRepet = new Email();
        $mailSemRepet = $mailSemRepet -> VerifMailRepet($mail); //Valida os e-mails repetidos recebidos via post/request.

        $mailOrdenados = new Email();
        $mailOrdenados = $mailOrdenados -> Sort($mailSemRepet); //Ordena o array em forma crescente.

        $gravaemail = implode("\n", $mailOrdenados);

        $timestamp = time(); // Cria o timestamp para utilização no nome do arquivo.

        $handle = fopen("emails_$timestamp.txt", "a");
        fwrite($handle, $gravaemail);
        {
            echo "\nArquivo TIMESTAMP salvo corretamente!";
        }
        fclose($handle);

        $handle = fopen("emails.txt", "a");
        fwrite($handle, $gravaemail);
        {
            echo "\nArquivo e-mails salvo corretamente!";
        }
        fclose($handle);
        unlink("temp.txt");
        {
            echo "\nArquivo Temp deletado!";
        }
    }
}
