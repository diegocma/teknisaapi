<?php

namespace App\Validations;

class Email
{
    public static function Filter()
    {
        $emailv = null;
        $verificatxt = fopen("temp.txt", "r"); //Analisa o arquivo temporário completo e faz as validações de e-mails.
        while (!feof($verificatxt))
        {
            $linha = fgets($verificatxt);
            $detalhe = explode('"', $linha);

            foreach($detalhe as $value)
            {
                if ($value != "emails" && $value != null && $value != ' : ') //Remove caracteres desnecessários na validação de e-mail.
                {
                    if (substr_count($value, '@') > 1) //Encontra mais de um e-mail na mesma linha.
                    {
                        if(stristr($value, '; ')) // Encontra '; ' na linha com mais de um e-mail.
                        {
                            $subdetalhe = explode('; ', $value); //Remove '; ' na mesma linha.

                            foreach ($subdetalhe as $subvalue)
                            {
                                if (filter_var($subvalue, FILTER_VALIDATE_EMAIL))
                                {
                                    $emailv = $emailv.$subvalue; //Grava os e-mails válidos em uma string;
                                    $emailv = $emailv."\n";
                                }
                            }
                        }
                        elseif (stristr($value, ' ')) // Encontra espaço na linha com mais de um e-mail.
                        {
                            $subdetalhe2 = explode(' ', $value); //Remove espaço na mesma linha.

                            foreach ($subdetalhe2 as $subvalue2)
                            {
                                if (filter_var($subvalue2, FILTER_VALIDATE_EMAIL))
                                {
                                    $emailv = $emailv.$subvalue2; //Grava os e-mails válidos em uma string;
                                    $emailv = $emailv."\n";
                                }
                            }
                        }
                    }
                    else
                    {
                        if(filter_var($value, FILTER_VALIDATE_EMAIL) )
                        {
                            $emailv = $emailv.$value; //Grava os e-mails válidos em uma string;
                            $emailv = $emailv."\n";
                        }
                    }
                }
            }
        } // Fim While
        fclose($verificatxt);

        return $emailv;
    }

    public function VerifMailRepet($mail)
    {
        $verifem = explode("\n", $mail);
        $emailfinal = array(null);

        for($I = 0; $I < count($verifem); $I++) //Inicia verificação de emails válidos repetidos.
        {
            $repete = false;

            for($J = 0; $J < count($emailfinal); $J++)
            {
                if( $verifem[$I] === $emailfinal[$J] )
                {
                    $repete = true;
                }
            }
            if(!$repete)
            {
                $emailfinal[$I] = $verifem[$I];
            }
        }
        return $emailfinal;
    }

    public static function Sort($mail2) {

        sort($mail2, SORT_STRING | SORT_FLAG_CASE); //Ordena o array em forma crescente.
        return $mail2;
    }
}
